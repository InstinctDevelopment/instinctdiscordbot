const { Command, util: { isFunction } } = require('klasa');
const has = (obj, key) => Object.prototype.hasOwnProperty.call(obj, key);
const Discord = require('discord.js')
const randomColor = require('randomcolor')
module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			aliases: ['commands','cmds'],
			guarded: true,
			description: 'Display help for a command',
			usage: '(Command:command)',
			extendedHelp: 'None Available'
		});

		this.createCustomResolver('command', (arg, possible, message) => {
			if (!arg || arg === '') return undefined;
			return this.client.arguments.get('command').run(arg, possible, message);
		});
	}

	async run(message, [command]) {
		const prefix = message.guildSettings.get('prefix');
		const helpEmbed = new Discord.MessageEmbed()
		.setTimestamp()
		.setColor(randomColor())
		.addField('Experiencing Problems?', `Join our [Server Here](https://discord.gg/Tep4zpS) for bot support, reporting bugs and making suggestions!`, true)
		.setFooter('Requested by '+message.author.tag, message.author.displayAvatarURL())
		.setAuthor(this.client.user.tag + ' - Your Knight in shining armour!', this.client.user.displayAvatarURL())
		const furtherHelp = new Discord.MessageEmbed()
		.setTimestamp()
		.setAuthor(this.client.user.tag + ' - Your Knight in shining armour!', this.client.user.displayAvatarURL())
		.setFooter('Requested by '+message.author.tag, message.author.displayAvatarURL())
		.setColor(randomColor())
		if (command) {
			furtherHelp.setAuthor(this.client.user.tag + ' - ' +command.name+'|'+command.category, this.client.user.displayAvatarURL())
				furtherHelp.addField('Command Description','`'+command.description+'`')
				furtherHelp.addField('Command Usage','`'+command.usage.fullUsage(message)+'`'),
				furtherHelp.addField('Extended help','`'+command.extendedHelp+'`')
				furtherHelp.addField('Command Aliases','`'+command.aliases+'`', true)
			return message.sendMessage(furtherHelp);
		}
		const help = await this.buildHelp(message);
		const categories = Object.keys(help);
		const helpMessage = [];
		for (let cat = 0; cat < categories.length; cat++) {
			const subCategories = Object.keys(help[categories[cat]]);
			for (let subCat = 0; subCat < subCategories.length; subCat++)  helpEmbed.addField(subCategories[subCat], help[categories[cat]][subCategories[subCat]].join(', '))

		}
		helpEmbed.setDescription(`My prefix on this server is:`+' `'+ `${prefix}` +'`'+ `\n Use `+'`'+ `${prefix}help [commandname]` +'` '+ `to view more information about any command!`)

		return message.channel.send(helpEmbed)
	}
	async buildHelp(message) {
		const help = {};

		const prefix = message.guildSettings.get('prefix');
		const commandNames = [...this.client.commands.keys()];
		const longest = commandNames.reduce((long, str) => Math.max(long, str.length), 0);

		await Promise.all(this.client.commands.map((command) =>
			this.client.inhibitors.run(message, command, true)
				.then(() => {
					if (!has(help, command.category)) help[command.category] = {};
					if (!has(help[command.category], command.subCategory)) help[command.category][command.subCategory] = [];
					const description = isFunction(command.description) ? command.description(message.language) : command.description;
					help[command.category][command.subCategory].push('`'+`${prefix}${command.name}`+'`')
				})
				.catch(() => {
					// noop
				})
		));

		return help;
	}

};
