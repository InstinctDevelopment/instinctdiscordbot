const { Command, util: { toTitleCase, codeBlock } } = require('klasa');
const Discord = require('discord.js')
const randomColor = require('randomcolor')
module.exports = class extends Command {

	constructor(...args) {
		super(...args, {
			runIn: ['text'],
			permissionLevel: 6,
			guarded: true,
			subcommands: true,
			description: '',
			usage: '[key:string]',
			usageDelim: ' '
        })
    }

        run(message, [key], list) {
            const settingsEmbed = new Discord.MessageEmbed()
            const entry = this.getPath(key);
            if (entry.type === 'Folder') {
                key ? `: ${key.split('.').map(toTitleCase).join('/')}` : '',
                settingsEmbed.setDescription(message.guild.settings.display(message, entry))
                return (message.channel.send(settingsEmbed))
            }
        }
        getPath(key) {
            const { schema } = this.client.gateways.get('guilds');
            if (!key) return schema;
            try {
                return schema.get(key);
            } catch (__) {
                return undefined;
            }
        }}